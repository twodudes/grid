extends Node

signal player_moved

const MovementDirection = MovementDirection.Movement

const reaction_time = 0.5
const swap_controls_wait_time = 3

var _elapsed_time_from_last_swap = 0.0
var _prev_controls_mapping = null
var _cur_controls_mapping = null
var _hidden = false

onready var controls_generator = $ControlsGenerator
onready var player = $Player
onready var swap_controls_timer = $SwapControlsTimer
onready var input_handler = $InputHandler

func _ready():
	swap_controls_timer.connect("timeout", self, "_swap_controls")
	reset()

func reset():
	_prev_controls_mapping = null
	_cur_controls_mapping = controls_generator.reset()
	player.handle_controls_swap(_cur_controls_mapping)

func restart():
	_elapsed_time_from_last_swap = 0.0
	swap_controls_timer.restart(swap_controls_wait_time)

func handle_user_swipe(direction):
	if _hidden:
		return
	player.handle_movement(_map_input_to_movement(direction))

func handle_player_movement(player_position):
	emit_signal("player_moved", player_position)

func hide_player():
	player.get_node("Model").hide()
	player.get_node("ControlsDisplayer").hide()
	_hidden = true

func show_player():
	player.get_node("Model").show()
	player.get_node("ControlsDisplayer").show()
	_hidden = false

func _map_input_to_movement(swipe_direction):
	if _prev_controls_mapping != null and _elapsed_time_from_last_swap < reaction_time:
		return _prev_controls_mapping[swipe_direction]
	else:
		return _cur_controls_mapping[swipe_direction]

func _swap_controls():
	_prev_controls_mapping = _cur_controls_mapping
	_cur_controls_mapping = controls_generator.swap_controls()
	player.handle_controls_swap(_cur_controls_mapping)
	_elapsed_time_from_last_swap = 0.0
	swap_controls_timer.restart(swap_controls_wait_time)

func _process(delta):
	_elapsed_time_from_last_swap += delta
