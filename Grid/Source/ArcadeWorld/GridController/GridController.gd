extends Node

signal grid_changed
signal grid_finished_rendering
signal grid_cleared

const _default_difficulty_wait_time = 2.0

var _difficulty_wait_time = _default_difficulty_wait_time
var _clearing = false

onready var grid_update_timer = $GridUpdateTimer
onready var grid_generator = $GridGenerator
onready var grid = $Grid

func _ready():
	grid_update_timer.connect("timeout", self, "update_grid")

func reset():
	var grid_start_nearest_row = grid_generator.reset()
	var grid_buffer = grid_start_nearest_row["grid"]
	for i in range(0, grid_buffer.size()):
		grid.render_tiles_at_offset(
			grid_buffer[i],
			grid_start_nearest_row["start"],
			grid_start_nearest_row["nearest_row"] + i
		)
	grid.clear_tiles_at_offset(
		0,
		0,
		grid_generator._nearest_row - 1,
		_difficulty_wait_time
	)

func clear():
	_clearing = true
	grid_update_timer.stop()
	grid.clear()

func handle_grid_changed():
	emit_signal("grid_changed")

func handle_grid_finished_rendering():
	if _clearing:
		return
	emit_signal("grid_finished_rendering")

func handle_grid_cleared():
	_clearing = false
	emit_signal("grid_cleared")

func get_tile(position):
	var grid_position = grid.world_to_grid(position)
	return grid.get_tile(grid_position)

func activate_tile(position):
	var grid_position = grid.world_to_grid(position)
	grid.activate_tile(grid_position)

func update_grid():
	_difficulty_wait_time -= 0.1
	if _difficulty_wait_time <= 1.5:
		_difficulty_wait_time = 1.5
	grid_update_timer.restart(_difficulty_wait_time)
	var nearest_row_width_start_offset = grid_generator.get_nearest_row_width_start_offset()
	grid.clear_tiles_at_offset(
		nearest_row_width_start_offset["width"],
		nearest_row_width_start_offset["start"],
		nearest_row_width_start_offset["offset"],
		_difficulty_wait_time
	)
	var next_row_start_offset = grid_generator.generate_next_row_start_offset()
	grid.render_tiles_at_offset(
		next_row_start_offset["row"], 
		next_row_start_offset["start"], 
		next_row_start_offset["offset"]
	)
