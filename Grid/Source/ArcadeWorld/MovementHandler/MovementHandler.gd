extends Node

signal player_lost

const Tile = TileDescription.Tile
const text_height = 1.1

var grid_controller = null
var score_controller = null

onready var floating_text = preload("res://Source/ArcadeWorld/FloatingText/TextViewport.tscn") 

func handle_player_movement(player_position):
	if _player_lost(player_position):
		emit_signal("player_lost")
		return
	_update_grid_if_player_is_near_the_further_edge(player_position)
	if _player_collected_crystal(player_position):
		score_controller.update_crystals()
	var tile = grid_controller.get_tile(player_position)
	var score = score_controller.update_score(tile)
	_render_tile_score(player_position, score)
	grid_controller.activate_tile(player_position)
		
func _update_grid_if_player_is_near_the_further_edge(player_position):
	var furthest_row_world_z = grid_controller.grid.grid_to_world(Vector3(0, 0, grid_controller.grid_generator.furthest_row)).z
	if abs(player_position.z - furthest_row_world_z) < 6:
		grid_controller.update_grid()

func _player_lost(player_position):
	var tile = grid_controller.get_tile(player_position)
	if tile == null or tile.id == Tile.EMPTY:
		return true
	else:
		return false

func _player_collected_crystal(player_position):
	var tile = grid_controller.get_tile(player_position)
	if tile == null:
		return false
	for child in tile.get_children():
		if child is Crystal:
			return true
	return false

func _render_tile_score(player_position, score):
	var text = floating_text.instance()
	text.amount = score
	add_child(text)
	text.translation = Vector3(player_position.x, player_position.y + text_height, player_position.z)
	
