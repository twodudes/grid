extends Spatial
class_name ArcadeWorld

const Swipe_Direction = SwipeDirection.Swipe
const MovementDirection = MovementDirection.Movement
const Tile = TileDescription.Tile
const streak_threshold = 10

var _restarting = false

onready var player_controller = $PlayerController
onready var grid_controller = $GridController
onready var game_over_screen = $GUI/GameOverScreen
onready var score_controller = $GUI/ScoreController
onready var leaderboard_handler = $LeaderboardHandler
onready var movement_handler = $MovementHandler
onready var movement_streak_handler = $MovementStreakHandler
onready var data_handler = $DataHandler
onready var play_again_button = $GUI/GameOverScreen/Panel/VBoxContainer/PlayAgain
onready var leaderbord_button = $GUI/GameOverScreen/Panel/VBoxContainer/Leaderboard
onready var camera = $Camera

var sphere = null

func _ready():
	camera.target = player_controller.player
	sphere.target = player_controller.player
	movement_handler.grid_controller = grid_controller
	movement_handler.score_controller = score_controller
	movement_streak_handler.score_controller = score_controller
	score_controller.init(data_handler)
	player_controller.input_handler.camera = camera
	player_controller.connect("player_moved", movement_streak_handler, "update_swipe_streak")
	player_controller.connect("player_moved", movement_handler, "handle_player_movement")
	movement_handler.connect("player_lost", self, "_handle_player_lost")
	grid_controller.connect("grid_changed", self, "_grid_changed")
	play_again_button.connect("button_up", self, "_play_again_button_up")
	leaderbord_button.connect("button_up", self, "_leaderboard_button_up")
	_init_background_size()
	grid_controller.reset()
	yield(grid_controller, "grid_finished_rendering")
	grid_controller.grid_update_timer.restart(grid_controller._default_difficulty_wait_time)
	player_controller.restart()

func _start_restart():
	if _restarting:
		return
	_restarting = true
	player_controller.hide_player()
	player_controller.reset()
	movement_streak_handler.reset()
	grid_controller.clear()
	yield(grid_controller, "grid_cleared")
	player_controller.player.set_translation(Vector3(0, 1.1, 0))
	camera.reset()
	sphere.reset()
	game_over_screen.show()

func _continue_restart():
	player_controller.restart()
	score_controller.reset_score()
	player_controller.show_player()
	grid_controller.reset()
	yield(grid_controller, "grid_finished_rendering")
	grid_controller.grid_update_timer.restart(grid_controller._default_difficulty_wait_time)
	_restarting = false
		
func _handle_player_lost():
	leaderboard_handler.post_score(score_controller.current_score())
	_start_restart()

func _grid_changed():
	var player_position = player_controller.player.translation
	var tile = grid_controller.get_tile(player_position)
	if tile == null or tile.id == Tile.EMPTY:
		_handle_player_lost()

func _init_background_size():
	var background = $Camera/Background
	var screen_size = get_viewport().size
	var background_z = background.translation.z
	var corner_left_up = camera.project_position(Vector2(0, 0), background_z)
	var corner_left_down = camera.project_position(Vector2(0, screen_size.y), background_z)
	var corner_right_up = camera.project_position(Vector2(screen_size.x, 0), background_z)
	var background_width = corner_left_up.distance_to(corner_left_down)
	var background_hight = corner_left_up.distance_to(corner_right_up)
	background.mesh.size = Vector2(background_width, background_hight * 1.1)
	background.show()

func _play_again_button_up():
	game_over_screen.hide()
	_continue_restart()

func _leaderboard_button_up():
	leaderboard_handler.open_game_center()
