extends Spatial

var tutorial_completed = false

var arcade_world = preload("res://Source/ArcadeWorld/ArcadeWorld.tscn")
var tutorial_world = preload("res://Source/TutorialWorld/TutorialWorld.tscn")

func _ready():
	if tutorial_completed:
		var arcade_world_instance = arcade_world.instance()
		arcade_world_instance.sphere = $SphereMesh
		add_child(arcade_world_instance)
	else:
		var tutorial_world_instance = tutorial_world.instance()
		tutorial_world_instance.sphere = $SphereMesh
		add_child(tutorial_world_instance)

func tutorial_completed():
	for child in get_children():
		if child is TutorialWorld:
			child.queue_free()
	var arcade_world_instance = arcade_world.instance()
	arcade_world_instance.sphere = $SphereMesh
	add_child(arcade_world_instance)
