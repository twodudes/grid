extends Node

const Tile = TileDescription.Tile
const Modifier = TileDescription.Modifier
const _nearest_row = -5

var grid_size = Vector3(7, 0, 11)
var furthest_row = _nearest_row + grid_size.z - 1
var start = - grid_size.x / 2

var _rng = RandomNumberGenerator.new()
var _row = grid_size.z - 1
var _grid = []

onready var tile_generator = $TileGenerator

func reset():
	_row = grid_size.z - 1
	furthest_row = _nearest_row + grid_size.z - 1
	_rng.randomize()
	_grid = _create_first(grid_size.x, grid_size.z, - _nearest_row + 1)
	return {"grid": _grid, "start": start, "nearest_row": _nearest_row}

func get_nearest_row_width_start_offset():
	return {"width": grid_size.x, "start": start, "offset": furthest_row - _grid.size() + 1}

func generate_next_row_start_offset():
	if _row == grid_size.z - 1:
		_grid = _create_new(grid_size.x, grid_size.z)
		_row = -1
	_row += 1
	furthest_row += 1
	_generate_crystals()
	return {"row": _grid[_row], "start": start, "offset": furthest_row}

func _generate_crystals():
	for x in range(start, start + _grid[_row].size() + 1):
		var tile = _grid[_row][x - start]
		if tile.tile_id == TileDescription.Tile.BASIC:
			var crystal_prob = _rng.randi_range(1, 10)
			if crystal_prob <= 1:
				_grid[_row][x - start].modifier = TileDescription.Modifier.CRYSTAL

func _add_tiles(array, start_i, start_j):
	var i = start_i
	var j = start_j
	while (i < array.size()):
		# can we go right?
		if j + 1 < array[i].size() && _rng.randi_range(0, 11) >= 7:
			j += 1
		# can we go start?
		elif j > 0 && _rng.randi_range(0, 11) >= 7:
			j -= 1
		# go forward
		else:
			if i < array.size() - 1:
				i += 1
			else:
				break
		# only if we haven't previously generated tiles there
		if array[i][j].tile_id == Tile.EMPTY:
			array[i][j] = tile_generator.get_next_tile_description()
	return array

func _create_first(width, height, fill):	
	var array = _create_2d_array(width, height, TileDescription.new())
	for i in range(0, fill):
		for j in range(0, array[0].size()):
			array[i][j] = TileDescription.new(Tile.BASIC, Modifier.NO)
	return _add_tiles(array, fill - 1, width / 2)

func _create_new(width, height):
	var new_array = _create_2d_array(width, height, TileDescription.new())
	var on_coordinate = 0
	for j in range(0, width):
		new_array[0][j] = TileDescription.new(Tile.BASIC, Modifier.NO)
	return _add_tiles(new_array, 0, on_coordinate)

func _create_2d_array(width, height, value):
	var a = []
	for y in range(height):
		a.append([])
		a[y].resize(width)
		for x in range(width):
			a[y][x] = value
	return a
