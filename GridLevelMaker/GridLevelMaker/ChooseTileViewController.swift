//
// Created by Mikhail Rakhmanovc on 19/08/2020.
// Copyright (c) 2020 Two Dudes. All rights reserved.
//

import Foundation
import AppKit

final class ChooseTileCell: NSCollectionViewItem {

    override func loadView() {
        view = NSView()
        view.wantsLayer = true
        view.layer?.backgroundColor = NSColor.black.cgColor
    }

    var tile: Tile?

    func configure(with tile: Tile) {
        self.tile = tile
        view.layer?.backgroundColor = tile.tileColor.cgColor
    }
}


class ChooseTileViewController: NSViewController {
    lazy var scrollView: NSScrollView = {
        let view = NSScrollView()
        self.view.addSubview(view, insets: NSEdgeInsets())
        view.documentView = collectionView

        return view
    }()

    lazy var collectionView: NSCollectionView = {
        let layout = NSCollectionViewFlowLayout()
        layout.minimumLineSpacing = 2
        layout.minimumInteritemSpacing = 2

        let collectionView = NSCollectionView()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.collectionViewLayout = layout
        collectionView.allowsMultipleSelection = false
        collectionView.backgroundColors = [.clear]
        collectionView.isSelectable = true
        collectionView.register(
            ChooseTileCell.self,
            forItemWithIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ChooseTileCell")
        )
        return collectionView
    }()

    var onCellSelected: ((Tile) -> Void)?

    let tiles: [Tile] = [EmptyTile(), BasicTile(), VanishingTile(), StartTile(), EndTile()]

    override func viewDidLoad() {
        super.viewDidLoad()

        scrollView.wantsLayer = true
    }
}

extension ChooseTileViewController: NSCollectionViewDataSource {
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        tiles.count
    }

    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        let cell = collectionView.makeItem(
            withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ChooseTileCell"),
            for: indexPath
        ) as! ChooseTileCell

        cell.configure(with: tiles[indexPath.item])

        return cell
    }
}

extension ChooseTileViewController: NSCollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        guard let indexPath = indexPaths.first,
              let cell = collectionView.item(at: indexPath) as? ChooseTileCell else {
            return
        }
        if let tile = cell.tile {
            onCellSelected?(tile)
        }
    }

    func collectionView(_ collectionView: NSCollectionView, didDeselectItemsAt indexPaths: Set<IndexPath>) {
        guard let indexPath = indexPaths.first,
              let cell = collectionView.item(at: indexPath) as? ChooseTileCell else {
            return
        }
    }

    func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> NSSize {
        NSSize(
            width: 40,
            height: 40
        )
    }
}