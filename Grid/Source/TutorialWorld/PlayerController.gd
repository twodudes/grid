extends Node

signal player_moved

const SwipeDirection = SwipeDirection.Swipe
const MovementDirection = MovementDirection.Movement

const reaction_time = 0.5
const swap_controls_wait_time = 3

var _elapsed_time_from_last_swap = 0.0
var _prev_controls_mapping = null
var _cur_controls_mapping = null
var _hidden = false
var _number_of_times_player_moved = 0
var _swap_controls_each_n_movements = 3
var tutorial_handler = null

onready var controls_generator = $ControlsGenerator
onready var player = $Player
onready var input_handler = $InputHandler

func _ready():
	pass

func reset():
	_prev_controls_mapping = null
	_cur_controls_mapping = controls_generator.reset()
	player.handle_controls_swap(_cur_controls_mapping)
	_show_hint()

func restart():
	_elapsed_time_from_last_swap = 0.0

func handle_user_swipe(direction):
	if _hidden:
		return
	var movement_direction = _map_input_to_movement(direction)
	if movement_direction == MovementDirection.UP:
		player.handle_movement(movement_direction)

func handle_player_movement(player_position):
	_number_of_times_player_moved += 1
	if _number_of_times_player_moved % _swap_controls_each_n_movements == 0:
		_swap_controls()
		_show_hint()
	emit_signal("player_moved", player_position)

func hide_player():
	player.get_node("Model").hide()
	player.get_node("ControlsDisplayer").hide()
	_hidden = true

func show_player():
	player.get_node("Model").show()
	player.get_node("ControlsDisplayer").show()
	_hidden = false

func _map_input_to_movement(SwipeDirection):
	if _prev_controls_mapping != null and _elapsed_time_from_last_swap < reaction_time:
		return _prev_controls_mapping[SwipeDirection]
	else:
		return _cur_controls_mapping[SwipeDirection]

func _swap_controls():
	_prev_controls_mapping = _cur_controls_mapping
	_cur_controls_mapping = controls_generator.swap_controls()
	player.handle_controls_swap(_cur_controls_mapping)
	_elapsed_time_from_last_swap = 0.0

func _process(delta):
	_elapsed_time_from_last_swap += delta

func _show_hint():
	var all_swipe_directions = [SwipeDirection.UP, SwipeDirection.RIGHT, SwipeDirection.LEFT, SwipeDirection.DOWN]
	for swipe_direction in all_swipe_directions:
		var movement_direction = _cur_controls_mapping[swipe_direction]
		if movement_direction == MovementDirection.UP:
			player.handle_swipe_assist(swipe_direction)
