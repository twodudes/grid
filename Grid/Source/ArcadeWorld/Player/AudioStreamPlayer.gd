extends AudioStreamPlayer

const music_folder = "res://Assets/Music/"
const track_base_name = "Cube_Test_Move_"
const num_of_tracks = 7

var _tracks = null

func _ready():
	_tracks = []
	for i in range(1, num_of_tracks + 1):
		_tracks.append(load(music_folder + track_base_name + "0" + String(i) + ".wav"))

func play_random_track():
	self.stream = _tracks[_pick_random_track_index()]
	play()

func _pick_random_track_index():
	return randi() % _tracks.size()
