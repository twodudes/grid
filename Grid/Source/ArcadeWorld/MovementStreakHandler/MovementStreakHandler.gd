extends Node

const text_height = 1.1

var current_streak = 0
var score_controller = null

var _min_duration = 1.0
var _last_swipe_timestamp = 0

# onready var floating_text = preload("res://Source/FloatingText/TextViewport.tscn") 

func reset():
	_min_duration = 1.0
	current_streak = 0
	score_controller.current_streak = current_streak
	_last_swipe_timestamp = 0

func update_swipe_streak(player_position):
	var current_timestamp = OS.get_unix_time()
	if _last_swipe_timestamp + _min_duration < current_timestamp: 
		current_streak = 0
		_last_swipe_timestamp = current_timestamp
		return
	_last_swipe_timestamp = current_timestamp
	current_streak += 1
	score_controller.current_streak = current_streak

func _process(delta):
	if current_streak == 0:
		return
	var current_timestamp = OS.get_unix_time()
	if _last_swipe_timestamp + _min_duration < current_timestamp:
		current_streak = 0
		score_controller.current_streak = current_streak

# func _render_msg(player_position, msg):
# 	var text = floating_text.instance()
# 	text.amount = msg
# 	add_child(text)
# 	text.translation = Vector3(player_position.x, player_position.y + text_height, player_position.z)
