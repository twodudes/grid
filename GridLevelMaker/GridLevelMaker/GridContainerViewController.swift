//
// Created by Mikhail Rakhmanovc on 19/08/2020.
// Copyright (c) 2020 Two Dudes. All rights reserved.
//

import Foundation
import AppKit

class GridContainerViewController: NSViewController {

    var gridViewController: GridViewController!
    var chooseTileViewController: ChooseTileViewController!
    var chooseSizeViewController: ChooseSizeViewController!
    var tileEditingViewController: TileEditingViewController!

    let fileEditor = LevelFileEditor()
    let defaults = UserDefaults.standard

    var currentFileUrl: URL? {
        didSet {
            defaults.set(currentFileUrl, forKey: "current.url")
        }
    }

    @IBAction func saveDocument(_ sender: Any) {
        guard let url = currentFileUrl else {
            saveDocumentAs(sender)
            return
        }
        saveLevel(path: url.path)
    }

    @IBAction func saveDocumentAs(_ sender: Any) {
        let panel = NSSavePanel()
        panel.allowedFileTypes = ["lvl"]
        panel.allowsOtherFileTypes = true
        panel.begin { result in
            if result == .OK {
                self.saveLevel(path: panel.url!.path)
                self.currentFileUrl = panel.url
            }
        }
    }

    @IBAction func openDocument(_ sender: Any) {
        let panel = NSOpenPanel()
        panel.allowedFileTypes = ["lvl"]
        panel.begin { result in
            if result == .OK {
                self.openLevel(path: panel.url!.path)
                self.currentFileUrl = panel.url
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        gridViewController = NSStoryboard(name: "Main", bundle: .main).instantiateController(withIdentifier: "GridViewController") as! GridViewController
        chooseTileViewController = NSStoryboard(name: "Main", bundle: .main).instantiateController(withIdentifier: "ChooseTileViewController") as! ChooseTileViewController
        chooseSizeViewController = NSStoryboard(name: "Main", bundle: .main).instantiateController(withIdentifier: "ChooseSizeViewController") as! ChooseSizeViewController
        tileEditingViewController = NSStoryboard(name: "Main", bundle: .main).instantiateController(withIdentifier: "TileEditingViewController") as! TileEditingViewController
        addChild(gridViewController)
        addChild(chooseTileViewController)
        addChild(chooseSizeViewController)
        addChild(tileEditingViewController)

        chooseSizeViewController.onButtonTapped = { width, height in
            let alert = NSAlert()
            alert.messageText = "Really wanna do this?"
            alert.addButton(withTitle: "Ok")
            alert.addButton(withTitle: "Cancel")
            let result = alert.runModal()
            if result == .alertFirstButtonReturn {
                self.currentFileUrl = nil
                self.gridViewController.createNewLevel(width: width, height: height)
            }
        }
        let stack = NSStackView()
        stack.orientation = .vertical
        stack.spacing = 10.0
        stack.addArrangedSubview(chooseSizeViewController.view)
        stack.addArrangedSubview(chooseTileViewController.view)

        view.addSubview(stack, constraints: [
            equal(\.leadingAnchor, of: view),
            equal(\.topAnchor, of: view),
            equal(\.bottomAnchor, of: view),
            equal(\.widthAnchor, constant: 82.0)
        ])
        view.addSubview(gridViewController.view, constraints: [
            equal(\.topAnchor, of: view),
            equal(\.bottomAnchor, of: view),
            equal(\.leadingAnchor, \.trailingAnchor, of: stack, constant: 10.0)
        ])
        view.addSubview(tileEditingViewController.view, constraints: [
            equal(\.trailingAnchor, of: view),
            equal(\.topAnchor, of: view),
            equal(\.bottomAnchor, of: view),
            equal(\.leadingAnchor, \.trailingAnchor, of: gridViewController.view, constant: 10.0),
            equal(\.widthAnchor, constant: 200.0)
        ])
        chooseTileViewController.onCellSelected = { tile in
            self.gridViewController.drawTile = tile
        }
        chooseSizeViewController.onModeChanged = { mode in
            self.gridViewController.mode = mode
        }
        gridViewController.onStartEditingTile = { tile, index in
            self.tileEditingViewController.startEditing(tile: tile, index: index)
        }
        gridViewController.onEndEditingTile = { tile, index in
            self.tileEditingViewController.endEditing()
        }
        tileEditingViewController.onEditMade = { tile, index in
            self.gridViewController.changeTile(tile: tile, index: index)
        }
        if let url = defaults.url(forKey: "current.url") {print(url.path)
            openLevel(path: url.path)
            currentFileUrl = url
        }
    }

    func saveLevel(path: String) {
        let grid = gridViewController.getGrid()
        let level = Level(grid: grid)
        fileEditor.save(level: level, path: path)
    }

    func openLevel(path: String) {
        guard let level = fileEditor.load(path: path) else {
            return
        }
        let grid = Grid(level: level)
        gridViewController.setGrid(grid: grid)
    }
}

extension GridContainerViewController: NSOpenSavePanelDelegate {
}
