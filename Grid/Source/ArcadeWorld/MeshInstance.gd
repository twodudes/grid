extends MeshInstance

var target = null
var _prev_target_position = Vector3(0, 1.1, 0)

onready var original_position = transform.origin

func _ready():
	$RotationPlayer.play("rotate") 

func reset():
	_prev_target_position = Vector3(0, 1.1, 0)
	set_translation(original_position)

func _process(delta):
	var target_position = target.get_global_transform().origin
	var offset = target_position - _prev_target_position 
	transform.origin += offset
	_prev_target_position = target_position
