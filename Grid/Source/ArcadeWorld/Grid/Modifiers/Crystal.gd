extends Spatial
class_name Crystal

onready var fadeout_player = $FadeoutPlayer

func _ready():
	$Mesh/AnimationPlayer.play("color")

func disappear():
	fadeout_player.play("fade_out")
	fadeout_player.connect("animation_finished", self, "_faded_out")

func _faded_out(anim_name):
	queue_free()
