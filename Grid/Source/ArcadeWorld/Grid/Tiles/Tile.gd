extends MeshInstance
class_name Tile

signal finished_rising
signal finished_dissapearing

const speed_min = 0.8
const speed_max = 1.0

var id = 1
var visited = false

var _is_rising = false
var _is_dissapearing = false
var _dissapear_time = null
var _rng = RandomNumberGenerator.new()

onready var _rise_tween = $RiseTween

func _ready():
	_rise_tween.connect("tween_completed", self, "_finish_rising")

func rise(pos):
	_is_rising = true
	_rng.randomize()
	var speed = _rng.randf_range(speed_min, speed_max)
	_rise_tween.interpolate_property(
		self, "translation",
		translation, pos,
		1.0 / speed, Tween.TRANS_SINE, Tween.EASE_IN_OUT
	)
	_rise_tween.start()

func dissapear(dissapear_time):
	if _is_dissapearing:
		return
	_dissapear_time = dissapear_time
	_is_dissapearing = true
	if not _is_rising:
		_start_dissapearing(null, null)
	else:
		_rise_tween.connect("tween_completed", self, "_start_dissapearing")

func _finish_rising(obj, key):
	_is_rising = false
	emit_signal("finished_rising", self)

func _start_dissapearing(obj, key):
	$DissapearPlayer.connect("animation_finished", self, "_finish_dissapearing")
	$DissapearPlayer.playback_speed = 1.0 / _dissapear_time
	$DissapearPlayer.play("dissapear")

func _finish_dissapearing(anim_name):
	emit_signal("finished_dissapearing", self)
