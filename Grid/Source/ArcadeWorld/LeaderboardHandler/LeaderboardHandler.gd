extends Node

signal leaderboard_closed
signal score_posted

const leaderboard_id = "com.twodudes.grid.leaderboard"

enum Result {
	NONE, 
	SCORE_POSTED, 
	LEADERBOARD_CLOSED 
}

onready var game_center = Engine.get_singleton("GameCenter")

func open_game_center():
	game_center.show_game_center({})
	
func post_score(score):
	var result = game_center.post_score({
			"score": score,
			"category": leaderboard_id,
		}
	)

func _check_events():
	while game_center.get_pending_event_count() > 0:
		var event = game_center.pop_pending_event()
		if event.type == "show_game_center":
			return Result.LEADERBOARD_CLOSED
		if event.type == "post_score":
			return Result.SCORE_POSTED
		
	return Result.NONE
