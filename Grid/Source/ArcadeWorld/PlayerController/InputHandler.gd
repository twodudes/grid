extends Node

const SwipeDirection = SwipeDirection.Swipe
const min_drag_length = 0.5
const max_diagonal_slope = 2.5

var camera = null
var _swipe_start_pos = null

onready var timer = $Timer
onready var controller = get_parent()

func _unhandled_input(event: InputEvent) -> void:
	if not event is InputEventScreenTouch:
		return
	var pos_relative_to_player = _projected_position(event.position)
	if event.pressed:
		_start_swipe_detection(pos_relative_to_player)
	elif not timer.is_stopped():
		_end_swipe_detection(pos_relative_to_player)

func _start_swipe_detection(position: Vector3) -> void:
	timer.start()
	_swipe_start_pos = position

func _end_swipe_detection(position: Vector3) -> void:
	timer.stop()
	var swipe_start_pos = Vector2(-_swipe_start_pos.x, -_swipe_start_pos.z)
	var swipe_end_pos = Vector2(-position.x, -position.z)
	var swipe = swipe_end_pos - swipe_start_pos
	if _slope_is_too_steep(swipe):
		return
	var swipe_direction = _calculate_swipe_direction(swipe)
	if not swipe_direction == null:
		controller.handle_user_swipe(swipe_direction)

func _slope_is_too_steep(vec) -> bool:
	var vec_normalized: Vector2 = vec.normalized()
	var vec_normalized_abs = _abs(vec_normalized)
	if vec_normalized_abs.x + vec_normalized_abs.y >= max_diagonal_slope:
		return true
	else:
		return false

func _calculate_swipe_direction(swipe):
	var swipe_normalized: Vector2 = swipe.normalized()
	var swipe_normalized_abs = _abs(swipe_normalized)
	if swipe_normalized_abs.x > min_drag_length and swipe_normalized_abs.x > swipe_normalized_abs.y:
		if swipe.x > 0:
			return SwipeDirection.RIGHT
		else:
			return SwipeDirection.LEFT
	elif swipe_normalized_abs.y > min_drag_length:
		if swipe.y > 0:
			return SwipeDirection.DOWN
		else:
			return SwipeDirection.UP

func _projected_position(position):
	var camera_from = camera.project_ray_origin(position)
	var camera_to = camera.project_ray_normal(position)
	var n = Vector3(0, 1, 0) # plane normal
	var p = camera_from # ray origin
	var v = camera_to # ray direction
	var d = 0 # distance of the plane from origin
	var t = - (n.dot(p) + d) / n.dot(v) # solving for plain/ray intersection
	return p + t * v
			

func _abs(vector):
	return Vector2(abs(vector.x), abs(vector.y))
