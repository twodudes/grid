extends Control

const SwipeDirection = SwipeDirection.Swipe
const start_finger_position = Vector2(64, 64)
const transform = Transform2D(
	Vector2(cos(20.0 * PI / 180.0), sin(20.0 * PI / 180.0)),
	Vector2(cos(-40.0 * PI / 180.0), sin(-40.0 * PI / 180.0)),
	 Vector2.ZERO
)

var speed = 1.0
onready var finger = $Frame/Finger
onready var tween = $Frame/Finger/Tween

func _ready():
	tween.connect("tween_completed", self, "_tween_completed")

func show_hint(swipe_direction):
	var dir = Vector2.ZERO
	match swipe_direction:
		SwipeDirection.LEFT:
			dir += Vector2(-128, 0)
		SwipeDirection.RIGHT:
			dir += Vector2(128, 0)
		SwipeDirection.UP:
			dir += Vector2(0, 128)
		SwipeDirection.DOWN:
			dir += Vector2(0, -128)
	var transformed_dir = transform.xform(dir)
	tween.interpolate_property(
		finger, "rect_position",
		start_finger_position, start_finger_position + transformed_dir,
		1.0 / speed, Tween.TRANS_SINE, Tween.EASE_IN_OUT
		)
	tween.start()

func _tween_completed(obj, key):
	finger.set_position(start_finger_position)
