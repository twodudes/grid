extends "res://Source/ArcadeWorld/Grid/Tiles/Tile.gd"
class_name VanishingTile

signal finished_vanishing

const vanishing_time = 2.0

var _is_vanishing = false

func _ready():
	id = 2

func vanish():
	if _is_vanishing:
		return
	_is_vanishing = true
	if not _is_rising:
		_start_vanishing(null, null)
	else:
		_rise_tween.connect("tween_completed", self, "_start_vanishing")

func dissapear(dissapear_time):
	if _is_vanishing:
		return
	self._dissapear_time = dissapear_time
	if self._is_dissapearing:
		return
	self._is_dissapearing = true
	if not self._is_rising:
		self._start_dissapearing(null, null)
	else:
		_rise_tween.connect("tween_completed", self, "_start_dissapearing")

func _start_vanishing(obj, key):
	$VanishPlayer.connect("animation_finished", self, "_finish_vanishing")
	$VanishPlayer.play("vanish")

func _finish_vanishing(anim_name):
	emit_signal("finished_vanishing", self)
