extends Node

const Tile = TileDescription.Tile

var current_streak = 0

var _score = 0
var _crystals = 0
var _max_score = 0
var _difficulty_wait_time = 3
var _data_handler = null

onready var score_label = $Score
onready var crystals_label = $Crystals

func init(data_handler):
	_data_handler = data_handler
	var data = _data_handler.load_data()
	_max_score = data["max_score"]
	_crystals = data["crystals"]
	crystals_label.text = String(_crystals)
	_display_score()

func reset_score():
	_max_score = max(_score, _max_score)
	_score = 0
	_display_score()

func current_score():
	return _score

func update_crystals():
	_crystals += 1
	crystals_label.text = String(_crystals)

func update_score(tile):
	var additional_score = int(current_streak * 0.25)
	match tile.id:
		Tile.BASIC:
			additional_score += 1
		Tile.VANISHING:
			additional_score += 1
	_score += additional_score
	_display_score()
	return additional_score

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_FOCUS_OUT:
		_data_handler.save_data(_max_score, _crystals)
		get_tree().quit() # default behavior

func _display_score():
	score_label.text = String(floor(_score)) + " / " + String(floor(_max_score))
