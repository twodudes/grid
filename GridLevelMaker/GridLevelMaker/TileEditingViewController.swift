//
// Created by Mikhail Rakhmanovc on 22/08/2020.
// Copyright (c) 2020 Two Dudes. All rights reserved.
//

import AppKit

final class TileEditingViewController: NSViewController {

    var currentTile: Tile?
    var index = -1
    var movementViewController: MovementEditingViewController!

    var onEditMade: ((Tile, Int) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        movementViewController = NSStoryboard(name: "Main", bundle: .main).instantiateController(withIdentifier: "MovementEditingViewController") as! MovementEditingViewController
        addChild(movementViewController)
        view.addSubview(movementViewController.view, insets: NSEdgeInsets())

        movementViewController.onChangeMovements = { patterns in
            guard var tile = self.currentTile else {
                return
            }
            print("changed")
            tile.controls = patterns
            self.currentTile = tile
            self.onEditMade?(tile, self.index)
        }
    }

    func startEditing(tile: Tile, index: Int) {
        print("started")
        movementViewController.endEditing()
        currentTile = tile
        print("currentTile is ", tile)
        self.index = index
        movementViewController.patterns = tile.controls
        movementViewController.startEditing()
    }

    func endEditing() {
        print("ended")
        movementViewController.endEditing()
    }
}