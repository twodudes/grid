//
//  GridViewController.swift
//  GridLevelMaker
//
//  Created by Mikhail Rakhmanovc on 19/08/2020.
//  Copyright © 2020 Two Dudes. All rights reserved.
//

import Cocoa
import AppKit

final class TileCell: NSCollectionViewItem {
    override func loadView() {
        view = NSView()
        view.wantsLayer = true
        view.layer?.backgroundColor = NSColor.black.cgColor
    }

    var index = 0
    var tile: Tile?

    func configure(with tile: Tile) {
        self.tile = tile
        view.layer?.backgroundColor = tile.tileColor.cgColor
        if tile.isSelected {
            view.layer?.borderWidth = 3.0
            view.layer?.borderColor = .white
        } else {
            view.layer?.borderColor = .clear
        }
    }
}

struct Grid {
    let width: Int
    let height: Int
    let tiles: [Tile]
}

extension Grid {
    init(level: Level) {
        self.width = level.width
        self.height = level.height
        self.tiles = level.tiles.map { $0.toTile() }
    }
}

class GridViewController: NSViewController {

    lazy var scrollView: NSScrollView = {
        let view = NSScrollView()
        self.view.addSubview(view, insets: NSEdgeInsets())
        view.documentView = collectionView

        return view
    }()

    lazy var collectionView: NSCollectionView = {
        let layout = NSCollectionViewFlowLayout()
        layout.minimumLineSpacing = 2
        layout.minimumInteritemSpacing = 2

        let collectionView = NSCollectionView()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.collectionViewLayout = layout
        collectionView.allowsMultipleSelection = false
        collectionView.backgroundColors = [.clear]
        collectionView.isSelectable = true
        collectionView.register(
            TileCell.self,
            forItemWithIdentifier: NSUserInterfaceItemIdentifier(rawValue: "TileCell")
        )
        return collectionView
    }()

    var drawTile: Tile?
    var mode = TileEditMode.tileDraw {
        didSet {
            gestureRecognizer.isEnabled = mode == .tileDraw
            var indexes: [Int] = []
            for index in 0..<tiles.count {
                if tiles[index].isSelected {
                    tiles[index].isSelected = false
                    indexes.append(index)
                }
            }
            collectionView.reloadItems(at: Set(indexes.map { IndexPath(item: $0, section: 0) }))
        }
    }

    private var tiles: [Tile] = []
    private var width = 0
    private var height = 0
    private var observerToken: NSObjectProtocol?
    private var gestureRecognizer: NSPanGestureRecognizer!

    var onStartEditingTile: ((Tile, Int) -> Void)?
    var onEndEditingTile: ((Tile, Int) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        scrollView.wantsLayer = true
        gestureRecognizer = NSPanGestureRecognizer(target: self, action: #selector(gestureRecognized))
        collectionView.addGestureRecognizer(gestureRecognizer)
        gestureRecognizer.delegate = self
        observerToken = collectionView.observe(\.frame, options: .new, changeHandler: { [weak self] _, change in
            if let val = change.newValue {
                DispatchQueue.main.async {
                    self?.collectionView.collectionViewLayout?.invalidateLayout()
                }
            }
        })
    }

    func createNewLevel(width: Int, height: Int) {
        self.width = width
        self.height = height
        self.tiles = []
        for _ in 0 ..< width * height {
            tiles.append(EmptyTile())
        }
        collectionView.reloadData()
    }

    func changeTile(tile: Tile, index: Int) {
        tiles[index] = tile
    }

    func setGrid(grid: Grid) {
        self.width = grid.width
        self.height = grid.height
        tiles = grid.tiles
        collectionView.reloadData()
    }
    
    func getGrid() -> Grid {
        Grid(width: width, height: height, tiles: tiles)
    }

    @objc func gestureRecognized(recognizer: NSPanGestureRecognizer) {
        switch recognizer.state {
        case .began, .changed:
            guard let drawTile = drawTile else {
                return
            }
            let location = recognizer.location(in: collectionView)
            for item in collectionView.visibleItems() {
                guard let item = item as? TileCell else { continue }
                if item.view.frame.contains(location) {
                    if item.tile?.tileType == drawTile.tileType {
                        return
                    }
                    item.configure(with: drawTile)
                    tiles[item.index] = drawTile
                }
            }
        default:
            break
        }
    }
}

extension GridViewController: NSGestureRecognizerDelegate {
}

extension GridViewController: NSCollectionViewDataSource {
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        tiles.count
    }

    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        let cell = collectionView.makeItem(
            withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "TileCell"),
            for: indexPath
        ) as! TileCell

        cell.index = indexPath.item
        cell.configure(with: tiles[indexPath.item])

        return cell
    }
}

extension GridViewController: NSCollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        guard let indexPath = indexPaths.first,
              let cell = collectionView.item(at: indexPath) as? TileCell else {
            return
        }
        if mode == .tileSelect {
            startEditingTile(tiles[indexPath.item], indexPath.item)
        } else if let tile = drawTile {
            if cell.tile?.tileType == tile.tileType {
                return
            }
            cell.configure(with: tile)
            tiles[cell.index] = tile
        }
    }

    func collectionView(_ collectionView: NSCollectionView, didDeselectItemsAt indexPaths: Set<IndexPath>) {
        guard let indexPath = indexPaths.first,
              let cell = collectionView.item(at: indexPath) as? TileCell else {
            return
        }
        if mode == .tileSelect {
            endEditingTile(tiles[indexPath.item], indexPath.item)
        }
    }

    func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> NSSize {
        let flow = collectionViewLayout as! NSCollectionViewFlowLayout
        let width = (view.frame.width / CGFloat(self.width)) - flow.minimumInteritemSpacing
        let size = NSSize(
            width: width,
            height: 40
        )
        return size
    }

    func startEditingTile(_ tile: Tile, _ index: Int) {
        tiles[index].isSelected = true
        if let item = collectionView.item(at: index) as? TileCell {
            item.configure(with: tiles[index])
        }
        onStartEditingTile?(tiles[index], index)
    }

    func endEditingTile(_ tile: Tile, _ index: Int) {
        tiles[index].isSelected = false
        if let item = collectionView.item(at: index) as? TileCell {
            item.configure(with: tiles[index])
        }
        onEndEditingTile?(tiles[index], index)
    }
}
