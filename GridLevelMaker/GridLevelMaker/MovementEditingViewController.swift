//
// Created by Mikhail Rakhmanovc on 22/08/2020.
// Copyright (c) 2020 Two Dudes. All rights reserved.
//

import AppKit

private func createTextField() -> NSTextField {
    let field = NSTextField()
    field.drawsBackground = false
    field.isBordered = false
    field.isEditable = false
    field.font = NSFont(name: "FiraCode-Medium", size: 16)
    return field
}

private func generateMovementPatterns() -> [MovementPattern] {
    let patterns = (0...10).map { _ in
        MovementPattern(string: "LURD")
    }
    return patterns + [MovementPattern(string: "RLUD")]
}

final class MovementPatternView: NSView {
    let left = createTextField()
    let right = createTextField()
    let up = createTextField()
    let down = createTextField()

    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        addSubview(left, constraints: [
            equal(\.centerYAnchor, of: self),
            equal(\.leadingAnchor, of: self)
        ])
        addSubview(right, constraints: [
            equal(\.centerYAnchor, of: self),
            equal(\.trailingAnchor, of: self)
        ])
        addSubview(up, constraints: [
            equal(\.centerXAnchor, of: self),
            equal(\.topAnchor, of: self)
        ])
        addSubview(down, constraints: [
            equal(\.centerXAnchor, of: self),
            equal(\.bottomAnchor, of: self)
        ])
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        addSubview(left, constraints: [
            equal(\.centerYAnchor, of: self),
            equal(\.leadingAnchor, of: self)
        ])
        addSubview(right, constraints: [
            equal(\.centerYAnchor, of: self),
            equal(\.trailingAnchor, of: self)
        ])
        addSubview(up, constraints: [
            equal(\.centerXAnchor, of: self),
            equal(\.topAnchor, of: self)
        ])
        addSubview(down, constraints: [
            equal(\.centerXAnchor, of: self),
            equal(\.bottomAnchor, of: self)
        ])
    }

    override var intrinsicContentSize: NSSize {
        return NSSize(width: 70, height: 70)
    }

    func configure(with pattern: MovementPattern) {
        left.stringValue = pattern.left.characterRepresentation
        right.stringValue = pattern.right.characterRepresentation
        up.stringValue = pattern.up.characterRepresentation
        down.stringValue = pattern.down.characterRepresentation
    }
}


final class MovementEditingViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource {

    var initialized = false
    let scrollView = NSScrollView()
    let tableView = NSTableView()

    var patterns: [MovementPattern] = [] {
        didSet {
            if isEditing {
                onChangeMovements?(patterns)
            }
        }
    }

    var onChangeMovements: (([MovementPattern]) -> Void)?
    var isEditing = false

    private var dragDropType = NSPasteboard.PasteboardType(rawValue: "private.table-row")

    lazy var textField: NSTextField = {
        let textField = NSTextField()
        textField.widthAnchor.constraint(equalToConstant: 100.0).isActive = true

        return textField
    }()

    lazy var addButton: NSButton = {
        let create = NSButton(title: "Add", target: self, action: #selector(addButtonTapped))
        create.target = self

        return create
    }()

    lazy var containerStackView: NSStackView = {
        let stack = NSStackView()
        stack.orientation = .vertical
        stack.addArrangedSubview(textField)
        stack.addArrangedSubview(addButton)
        stack.addArrangedSubview(scrollView)

        view.addSubview(stack, insets: NSEdgeInsets())

        return stack
    }()

    @objc func addButtonTapped() {
        let pattern = MovementPattern(string: textField.stringValue)
        patterns.insert(pattern, at: 0)
        tableView.insertRows(at: IndexSet([0]), withAnimation: .effectFade)
    }

    func endEditing() {
        isEditing = false
        patterns = []
        tableView.reloadData()
    }

    func startEditing() {
        isEditing = true
        tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidLayout() {
        if !initialized {
            initialized = true
            setupTableView()
            setupView()
        }
    }

    func setupView() {
        view.translatesAutoresizingMaskIntoConstraints = false
        containerStackView.wantsLayer = true
    }

    func setupTableView() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.headerView = nil
        scrollView.backgroundColor = NSColor.clear
        scrollView.drawsBackground = false
        tableView.backgroundColor = NSColor.clear
        tableView.appearance = NSAppearance(named: NSAppearance.Name.vibrantDark)
        tableView.registerForDraggedTypes([dragDropType])
        let col = NSTableColumn(identifier: NSUserInterfaceItemIdentifier(rawValue: "col"))
        col.minWidth = 100
        tableView.addTableColumn(col)

        scrollView.documentView = tableView
        scrollView.hasHorizontalScroller = false
        scrollView.hasVerticalScroller = true
    }

    func numberOfRows(in tableView: NSTableView) -> Int {
        return patterns.count
    }

    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let cell = NSTableCellView()
        let view = MovementPatternView()
        view.configure(with: patterns[row])
        cell.addSubview(view, constraints: [
            equal(\.centerYAnchor, of: cell),
            equal(\.centerXAnchor, of: cell),
        ])

        return cell
    }

    func tableView(_ tableView: NSTableView, rowViewForRow row: Int) -> NSTableRowView? {
        let rowView = NSTableRowView()
        rowView.isEmphasized = false
        return rowView
    }

    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: NSTableView, pasteboardWriterForRow row: Int) -> NSPasteboardWriting? {
        let item = NSPasteboardItem()
        item.setString(String(row), forType: self.dragDropType)
        return item
    }

    func tableView(_ tableView: NSTableView, validateDrop info: NSDraggingInfo, proposedRow row: Int, proposedDropOperation dropOperation: NSTableView.DropOperation) -> NSDragOperation {
        if dropOperation == .above {
            return .move
        }
        return []
    }

    func tableView(_ tableView: NSTableView, objectValueFor tableColumn: NSTableColumn?, row: Int) -> Any? {
        return nil
    }

    func tableView(_ tableView: NSTableView, rowActionsForRow row: Int, edge: NSTableView.RowActionEdge) -> [NSTableViewRowAction] {
        // left swipe
        if edge == .trailing {
            let deleteAction = NSTableViewRowAction(style: .destructive, title: "Delete", handler: { (rowAction, row) in
                // action code
                tableView.removeRows(at: IndexSet(integer: row), withAnimation: .effectFade)
                self.patterns.remove(at: row)
            })
            deleteAction.backgroundColor = NSColor.red
            return [deleteAction]
        }
        return []
    }

    func tableView(_ tableView: NSTableView, acceptDrop info: NSDraggingInfo, row: Int, dropOperation: NSTableView.DropOperation) -> Bool {
        var oldIndexes = [Int]()
        info.enumerateDraggingItems(options: [], for: tableView, classes: [NSPasteboardItem.self], searchOptions: [:]) { dragItem, _, _ in
            if let str = (dragItem.item as! NSPasteboardItem).string(forType: self.dragDropType), let index = Int(str) {
                oldIndexes.append(index)
            }
        }

        var oldIndexOffset = 0
        var newIndexOffset = 0

        // For simplicity, the code below uses `tableView.moveRowAtIndex` to move rows around directly.
        // You may want to move rows in your content array and then call `tableView.reloadData()` instead.
        if !oldIndexes.isEmpty {
            let oldPattern = patterns[oldIndexes[0]]
            patterns.remove(at: oldIndexes[0])
            patterns.insert(oldPattern, at: row)
        }
        tableView.beginUpdates()
        for oldIndex in oldIndexes {
            if oldIndex < row {
                tableView.moveRow(at: oldIndex + oldIndexOffset, to: row - 1)
                oldIndexOffset -= 1
            } else {
                tableView.moveRow(at: oldIndex, to: row + newIndexOffset)
                newIndexOffset += 1
            }
        }
        tableView.endUpdates()

        return true
    }
}
