class_name TileDescription

enum Tile {EMPTY = -1, BASIC = 1, VANISHING = 2}
enum Modifier {NO = 1, CRYSTAL = 2}

var tile_id: int
var modifier: int
	
func _init(Tile = -1, modifier = 1):
	tile_id = Tile
	modifier = modifier
