extends Node

const MovementDirection = MovementDirection.Movement
const default_controls_mapping = [
	MovementDirection.LEFT,
	MovementDirection.RIGHT,
	MovementDirection.UP,
	MovementDirection.DOWN,
]
const swap_patterns_by_difficulty = {
	0: [
		# up
			# right
		[MovementDirection.LEFT, MovementDirection.RIGHT, MovementDirection.UP, MovementDirection.DOWN],
	],
	1: [
		# up
			# right
		[MovementDirection.DOWN, MovementDirection.RIGHT, MovementDirection.UP, MovementDirection.LEFT],
			# left
		[MovementDirection.RIGHT, MovementDirection.LEFT, MovementDirection.UP, MovementDirection.DOWN],
		# right
			# up
		[MovementDirection.LEFT, MovementDirection.UP, MovementDirection.RIGHT, MovementDirection.DOWN],
		[MovementDirection.DOWN, MovementDirection.UP, MovementDirection.RIGHT, MovementDirection.LEFT],
			# down
		[MovementDirection.UP, MovementDirection.DOWN, MovementDirection.RIGHT, MovementDirection.LEFT],
		# left
			# up
		[MovementDirection.DOWN, MovementDirection.UP, MovementDirection.LEFT, MovementDirection.RIGHT],
			# down
		[MovementDirection.UP, MovementDirection.DOWN, MovementDirection.LEFT, MovementDirection.RIGHT],
		# down
			# right
		[MovementDirection.LEFT, MovementDirection.RIGHT, MovementDirection.DOWN, MovementDirection.UP],
			# left
		[MovementDirection.RIGHT, MovementDirection.LEFT, MovementDirection.DOWN, MovementDirection.UP],
	],
	2: [
		# up
			# left
		[MovementDirection.DOWN, MovementDirection.LEFT, MovementDirection.UP, MovementDirection.RIGHT],
			# down
		[MovementDirection.LEFT, MovementDirection.DOWN, MovementDirection.UP, MovementDirection.RIGHT],
		[MovementDirection.RIGHT, MovementDirection.DOWN, MovementDirection.UP, MovementDirection.LEFT],
		# right
			# down
		[MovementDirection.LEFT, MovementDirection.DOWN, MovementDirection.RIGHT, MovementDirection.UP],
			# left
		[MovementDirection.DOWN, MovementDirection.LEFT, MovementDirection.RIGHT, MovementDirection.UP],
		[MovementDirection.UP, MovementDirection.LEFT, MovementDirection.RIGHT, MovementDirection.DOWN],
		# left
			# up
		[MovementDirection.RIGHT, MovementDirection.UP, MovementDirection.LEFT, MovementDirection.DOWN],
			# down
		[MovementDirection.RIGHT, MovementDirection.DOWN, MovementDirection.LEFT, MovementDirection.UP],
			# right
		[MovementDirection.DOWN, MovementDirection.RIGHT, MovementDirection.LEFT, MovementDirection.UP],
		[MovementDirection.UP, MovementDirection.RIGHT, MovementDirection.LEFT, MovementDirection.DOWN],
		# down
			# right
		[MovementDirection.UP, MovementDirection.RIGHT, MovementDirection.DOWN, MovementDirection.LEFT],
			# left
		[MovementDirection.UP, MovementDirection.LEFT, MovementDirection.DOWN, MovementDirection.RIGHT],
			# up
		[MovementDirection.RIGHT, MovementDirection.UP, MovementDirection.DOWN, MovementDirection.LEFT],
		[MovementDirection.LEFT, MovementDirection.UP, MovementDirection.DOWN, MovementDirection.RIGHT],
	],
}
const intro_controls_difficulty_sequence = [1, 1, 1, 1]

var _current_controls_difficulty_sequence = intro_controls_difficulty_sequence
var _current_swap_pattern_index = 0
var _rng = RandomNumberGenerator.new()

func _ready():
	_rng.randomize()

func reset():
	_rng.randomize()
	_current_controls_difficulty_sequence = intro_controls_difficulty_sequence
	_current_swap_pattern_index = 0
	return default_controls_mapping

func swap_controls():
	if _current_controls_difficulty_sequence.empty():
		_current_controls_difficulty_sequence = _generate_new_controls_difficulty_sequence()
	var difficulty = _current_controls_difficulty_sequence.pop_front()
	var current_swap_patterns = swap_patterns_by_difficulty[difficulty]
	_current_swap_pattern_index = _generate_index_for_current_swap_patterns(current_swap_patterns)
	return current_swap_patterns[_current_swap_pattern_index]

func _generate_new_controls_difficulty_sequence():
	return [2, 1, 2, 1, 2, 1]

func _generate_index_for_current_swap_patterns(swap_patterns) -> int:
	var swap_patterns_size = swap_patterns.size() - 1
	var index = 0
	if swap_patterns_size != 0:
		while true:
			index = _rng.randi_range(0, swap_patterns_size)
			if index != _current_swap_pattern_index:
				break
	return index

# ---

# const all_swap_patterns = [
# 	# up
# 		# right
# 	[MovementDirection.LEFT, MovementDirection.RIGHT, MovementDirection.UP, MovementDirection.DOWN],
# 	[MovementDirection.DOWN, MovementDirection.RIGHT, MovementDirection.UP, MovementDirection.LEFT],
# 		# left
# 	[MovementDirection.RIGHT, MovementDirection.LEFT, MovementDirection.UP, MovementDirection.DOWN],
# 	[MovementDirection.DOWN, MovementDirection.LEFT, MovementDirection.UP, MovementDirection.RIGHT],
# 		# down
# 	[MovementDirection.LEFT, MovementDirection.DOWN, MovementDirection.UP, MovementDirection.RIGHT],
# 	[MovementDirection.RIGHT, MovementDirection.DOWN, MovementDirection.UP, MovementDirection.LEFT],
# 	# right
# 		# up
# 	[MovementDirection.LEFT, MovementDirection.UP, MovementDirection.RIGHT, MovementDirection.DOWN],
# 	[MovementDirection.DOWN, MovementDirection.UP, MovementDirection.RIGHT, MovementDirection.LEFT],
# 		# down
# 	[MovementDirection.UP, MovementDirection.DOWN, MovementDirection.RIGHT, MovementDirection.LEFT],
# 	[MovementDirection.LEFT, MovementDirection.DOWN, MovementDirection.RIGHT, MovementDirection.UP],
# 		# left
# 	[MovementDirection.DOWN, MovementDirection.LEFT, MovementDirection.RIGHT, MovementDirection.UP],
# 	[MovementDirection.UP, MovementDirection.LEFT, MovementDirection.RIGHT, MovementDirection.DOWN],
# 	# left
# 		# up
# 	[MovementDirection.RIGHT, MovementDirection.UP, MovementDirection.LEFT, MovementDirection.DOWN],
# 	[MovementDirection.DOWN, MovementDirection.UP, MovementDirection.LEFT, MovementDirection.RIGHT],
# 		# down
# 	[MovementDirection.UP, MovementDirection.DOWN, MovementDirection.LEFT, MovementDirection.RIGHT],
# 	[MovementDirection.RIGHT, MovementDirection.DOWN, MovementDirection.LEFT, MovementDirection.UP],
# 		# right
# 	[MovementDirection.DOWN, MovementDirection.RIGHT, MovementDirection.LEFT, MovementDirection.UP],
# 	[MovementDirection.UP, MovementDirection.RIGHT, MovementDirection.LEFT, MovementDirection.DOWN],
# 	# down
# 		# right
# 	[MovementDirection.LEFT, MovementDirection.RIGHT, MovementDirection.DOWN, MovementDirection.UP],
# 	[MovementDirection.UP, MovementDirection.RIGHT, MovementDirection.DOWN, MovementDirection.LEFT],
# 		# left
# 	[MovementDirection.RIGHT, MovementDirection.LEFT, MovementDirection.DOWN, MovementDirection.UP],
# 	[MovementDirection.UP, MovementDirection.LEFT, MovementDirection.DOWN, MovementDirection.RIGHT],
# 		# up
# 	[MovementDirection.RIGHT, MovementDirection.UP, MovementDirection.DOWN, MovementDirection.LEFT],
# 	[MovementDirection.LEFT, MovementDirection.UP, MovementDirection.DOWN, MovementDirection.RIGHT],
# ]