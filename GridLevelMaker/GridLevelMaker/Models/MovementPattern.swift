//
// Created by Mikhail Rakhmanovc on 22/08/2020.
// Copyright (c) 2020 Two Dudes. All rights reserved.
//

import Foundation

enum MovementDirection: Int {
    case left = 0
    case right
    case up
    case down
}

extension MovementDirection: Codable {
    init(character: Character) {
        switch character {
        case "L", "l":
            self = .left
        case "R", "r":
            self = .right
        case "U", "u":
            self = .up
        case "D", "d":
            self = .down
        default:
            self = .left
        }
    }

    var characterRepresentation: String {
        switch self {
        case .left:
            return "←"
        case .right:
            return "→"
        case .up:
            return "↑"
        case .down:
            return "↓"
        }
    }
}

struct MovementPattern: Codable {
    let left: MovementDirection
    let right: MovementDirection
    let up: MovementDirection
    let down: MovementDirection

    init(string: String) {
        var directions = string.map(MovementDirection.init)
        if directions.count != 4 {
            directions = [.left, .up, .right, .down]
        }
        left = directions[0]
        right = directions[2]
        up = directions[1]
        down = directions[3]
    }
}