extends Node

const Tile = TileDescription.Tile
const Modifier = TileDescription.Modifier

var _rng = RandomNumberGenerator.new()
var _descriptions = []
var _tile_probabilities = {
	Tile.BASIC: 0.6,
	Tile.VANISHING: 0.4
}
var _tile_count_ranges = {
	Tile.BASIC: [4, 10],
	Tile.VANISHING: [4, 7]
}
var _tile_batch_ranges = {
	Tile.BASIC: [4, 6],
	Tile.VANISHING: [2, 4]
}
var _all_tile_types = [Tile.BASIC, Tile.VANISHING]
var _tile_probabilities_array = []
var _tiles_buffer = []
var _current_index = 0

func _ready():
	_rng.randomize()
	_tile_probabilities_array = _gen_tile_probabilities_array()
	_gen_tiles_buffer()

func get_next_tile_description():
	if _current_index >= _tiles_buffer.size():
		_gen_tiles_buffer()
		_current_index = 0
	var res = _tiles_buffer[_current_index]
	_current_index += 1
	return res

func _gen_tiles_buffer():
	_tiles_buffer = []
	var cell_batches = []
	for Tile in _all_tile_types:
		var number_of_cells = _rng.randi_range(_tile_batch_ranges[Tile][0], _tile_batch_ranges[Tile][1])
		for i in range(0, number_of_cells):
			cell_batches.append(Tile)
	cell_batches.shuffle()
	for Tile in cell_batches:
		var count = _rng.randi_range(_tile_count_ranges[Tile][0], _tile_count_ranges[Tile][1])
		for j in range(0, count):
			_tiles_buffer.append(TileDescription.new(Tile, Modifier.NO))

func _gen_tile_probabilities_array():
	var start = 0.0
	var tile_probabilities_array = []
	for Tile in _all_tile_types:
		tile_probabilities_array.append([start, Tile])
		start += _tile_probabilities[Tile]
	return tile_probabilities_array

