extends Position2D

signal finished_animating

onready var label = get_node("Label")
onready var tween = get_node("Tween")

var amount = 200
var velocity = Vector2(0.0, 0.0)

func start():
	label.set_text(str(amount))
	tween.connect("tween_all_completed", self, "_tween_completed")
	tween.interpolate_property(
		self,
		"scale", 
		scale, 
		Vector2(0.1, 0.1), 
		0.7, 
		Tween.TRANS_LINEAR, 
		Tween.EASE_OUT,
		0.2
	)
	tween.start()
	

func _tween_completed():
	emit_signal("finished_animating")
	self.queue_free()
