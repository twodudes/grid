//
// Created by Mikhail Rakhmanovc on 21/08/2020.
// Copyright (c) 2020 Two Dudes. All rights reserved.
//

import AppKit
import Foundation

enum TileEditMode: String {
    case tileDraw = "Tile Draw"
    case tileSelect = "Tile Select"

    func toggle() -> TileEditMode {
        if self == .tileDraw {
            return .tileSelect
        }
        return .tileDraw
    }
}

final class ChooseSizeViewController: NSViewController {

    lazy var widthTextField: NSTextField = {
        let textField = NSTextField()
        textField.widthAnchor.constraint(equalToConstant: 60.0).isActive = true

        return textField
    }()

    lazy var heightTextField: NSTextField = {
        let textField = NSTextField()
        textField.widthAnchor.constraint(equalToConstant: 60.0).isActive = true

        return textField
    }()

    lazy var createButton: NSButton = {
        let create = NSButton(title: "Create", target: self, action: #selector(createButtonTapped))
        create.title = "Create"
        create.target = self

        return create
    }()

    lazy var changeMode: NSButton = {
        let create = NSButton(title: mode.rawValue, target: self, action: #selector(changeButtonTapped))
        create.target = self

        return create
    }()

    lazy var containerStackView: NSStackView = {
        let stack = NSStackView()
        stack.orientation = .vertical
        stack.addArrangedSubview(widthTextField)
        stack.addArrangedSubview(heightTextField)
        stack.addArrangedSubview(createButton)
        stack.addArrangedSubview(changeMode)

        view.addSubview(stack, insets: NSEdgeInsets())

        return stack
    }()

    var onButtonTapped: ((Int, Int) -> Void)?
    var onModeChanged: ((TileEditMode) -> Void)?
    var mode = TileEditMode.tileDraw

    override func viewDidLoad() {
        super.viewDidLoad()

        containerStackView.wantsLayer = true
    }

    override func viewDidAppear() {
        super.viewDidAppear()

        print(containerStackView.frame)
    }

    @objc func createButtonTapped() {
        let width = min(abs(Int(widthTextField.stringValue) ?? 10), 100)
        let height = min(abs(Int(heightTextField.stringValue) ?? 10), 200)

        onButtonTapped?(width, height)
    }

    @objc func changeButtonTapped() {
        mode = mode.toggle()
        changeMode.title = mode.rawValue
        onModeChanged?(mode)
    }
}