//
// Created by Mikhail Rakhmanovc on 19/08/2020.
// Copyright (c) 2020 Two Dudes. All rights reserved.
//

import AppKit

enum TileType: Int {
    case none = 0
    case basic
    case vanishing
    case moving
    case start
    case end
}

protocol TileData {}

protocol Tile {
    var tileType: TileType { get }
    var tileColor: NSColor { get }

    var controls: [MovementPattern] { get set }
    var isSelected: Bool { get set }
}

struct EmptyTile: Tile {
    var tileType: TileType {
        .none
    }

    var tileColor: NSColor {
        .black
    }

    var controls: [MovementPattern] = []

    var isSelected = false
}

struct BasicTile: Tile {
    var tileType: TileType {
        .basic
    }

    var tileColor: NSColor {
        .green
    }

    var controls: [MovementPattern] = []

    var isSelected = false
}

struct VanishingTile: Tile {
    var tileType: TileType {
        .vanishing
    }

    var tileColor: NSColor {
        .red
    }

    var controls: [MovementPattern] = []

    var isSelected = false
}

struct MovingTile: Tile {
    var tileType: TileType {
        .moving
    }

    var tileColor: NSColor {
        .blue
    }

    var controls: [MovementPattern] = []

    var isSelected = false
}

struct StartTile: Tile {
    var tileType: TileType {
        .start
    }

    var tileColor: NSColor {
        .yellow
    }

    var controls: [MovementPattern] = []

    var isSelected = false
}

struct EndTile: Tile {
    var tileType: TileType {
        .end
    }

    var tileColor: NSColor {
        .purple
    }

    var controls: [MovementPattern] = []

    var isSelected = false
}

