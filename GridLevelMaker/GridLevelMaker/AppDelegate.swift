//
//  AppDelegate.swift
//  GridLevelMaker
//
//  Created by Mikhail Rakhmanovc on 19/08/2020.
//  Copyright © 2020 Two Dudes. All rights reserved.
//

import Cocoa


@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
    // Insert code here to initialize your application
    }


    func applicationWillTerminate(_ aNotification: Notification) {
    // Insert code here to tear down your application
    }



}
