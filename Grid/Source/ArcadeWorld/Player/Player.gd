extends Spatial

const MovementDirection = MovementDirection.Movement
const step_length = 2

var player_speed = 8.0

onready var model = $Model
onready var tween = $Tween
onready var controls_displayer = $ControlsDisplayer
onready var controller = get_parent()

func _ready():
	$Model/AnimationPlayer.play("color")
	tween.connect("tween_all_completed", self, "_tween_all_completed")

func handle_movement(movement_dir):
	if tween.is_active():
		return
	var end_pos = translation
	var rotation_vector: Vector3
	match movement_dir:
		MovementDirection.LEFT:
			end_pos += Vector3(step_length, 0, 0)
			rotation_vector =  Vector3(0, 0, deg2rad(-90))
		MovementDirection.RIGHT:
			end_pos += Vector3(-step_length, 0, 0)
			rotation_vector =  Vector3(0, 0, deg2rad(90))
		MovementDirection.UP:
			end_pos += Vector3(0, 0, step_length)
			rotation_vector =  Vector3(deg2rad(90), 0, 0)
		MovementDirection.DOWN:
			end_pos += Vector3(0, 0, -step_length)
			rotation_vector =  Vector3(deg2rad(-90), 0, 0)
	_move_and_rotate(end_pos, rotation_vector)

func handle_controls_swap(controls_mapping):
	controls_displayer.update_controls(controls_mapping)

func handle_swipe_assist(dir):
	controls_displayer.animate_direction(dir)

func _move_and_rotate(end_pos, rotation_vector):
	$AudioStreamPlayer.play_random_track()
	tween.interpolate_property(
		self, "translation",
		translation, end_pos,
		1.0 / player_speed, Tween.TRANS_SINE, Tween.EASE_IN_OUT
	)
	tween.interpolate_property(
		model, "rotation",
		rotation, rotation + rotation_vector,
		1.0 / player_speed, Tween.TRANS_SINE, Tween.EASE_IN_OUT
	)
	tween.start()

func _tween_all_completed():
	controller.handle_player_movement(translation)

