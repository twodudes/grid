extends Spatial

signal finished_animating

onready var label = get_node("Viewport/Node2D/FloatingText")
onready var tween = get_node("Tween")

var amount = 200
var velocity = Vector2(0.0, 0.0)

func _ready():
	randomize()
	label.amount = amount
	label.connect("finished_animating", self, "_finish_animating")
	label.start()
	var side_movement = -rand_range(1.0, 2.0)
	var _sign = (randi() % 2) == 0
	if _sign:
		side_movement = -side_movement
	velocity = Vector3(0, 3.0, side_movement)
	

func _finish_animating():
	self.queue_free()
	

func _process(delta):
	translation += velocity * delta
