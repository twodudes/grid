extends Spatial

const SwipeDirection = SwipeDirection.Swipe
const MovementDirection = MovementDirection.Movement
const animation_time = 0.4
const all_swipe_directions = [
	SwipeDirection.LEFT, 
	SwipeDirection.RIGHT, 
	SwipeDirection.UP, 
	SwipeDirection.DOWN,
]

var _is_animating = false
var _arrow_quads = []
var _current_controls_mapping = {}
var symmetrical_swipes = {
	SwipeDirection.UP: SwipeDirection.DOWN,
	SwipeDirection.DOWN: SwipeDirection.UP,
	SwipeDirection.LEFT: SwipeDirection.RIGHT,
	SwipeDirection.RIGHT: SwipeDirection.LEFT,
}
var _arrow_quads_transforms = {
	MovementDirection.UP: Vector3(0, 0, 0),
	MovementDirection.DOWN: Vector3(0, PI, 0),
	MovementDirection.LEFT: Vector3(0, PI / 2, 0),
	MovementDirection.RIGHT: Vector3(0, 3 * PI / 2, 0),
}
var _next_controls_mapping = []
var _next_animation_time = []
var _next_swipe_direction = null

var _prev_node = null
var _prev_translation = null

onready var up = get_node("Up")
onready var down = get_node("Down")
onready var left = get_node("Left")
onready var right = get_node("Right")
onready var tween = get_node("Tween")

func _ready():
	_arrow_quads = [left, right, up, down]
	tween.connect("tween_all_completed", self, "_tween_all_completed")

func update_controls(controls):
	if _is_animating:
		_next_controls_mapping.append(controls)
		_next_animation_time.append(animation_time)
		return
	_current_controls_mapping = controls
	var material = null
	for swipe in all_swipe_directions:
		var dir = controls[swipe]
		var new_transform = _arrow_quads_transforms[dir]
		var symmetrical_swipe = symmetrical_swipes[swipe]
		var symmetrical_node = _get_node_by_position(symmetrical_swipe)
		material = symmetrical_node.get_node("MeshInstance").mesh.surface_get_material(0)
		_change_control(tween, symmetrical_node, new_transform)
	_animate_material(tween, material)
	_is_animating = true
	tween.start()

func _get_node_by_position(swipe):
	for node in _arrow_quads:
		if swipe == SwipeDirection.LEFT && node.translation.x > 0:
			return node
		if swipe == SwipeDirection.RIGHT && node.translation.x < 0:
			return node
		if swipe == SwipeDirection.UP && node.translation.z > 0:
			return node
		if swipe == SwipeDirection.DOWN && node.translation.z < 0:
			return node

func animate_direction(swipe):
	_next_swipe_direction = swipe
	if _is_animating:
		return
	var node = _get_node_by_position(swipe)
	_prev_node = node
	_prev_translation = node.translation
	tween.interpolate_property(
		node,
		"translation",
		node.translation,
		node.translation * 2.0,
		0.7,
		Tween.TRANS_SINE, 
		Tween.EASE_IN_OUT
	)
	tween.interpolate_property(
		node,
		"scale",
		node.scale,
		node.scale * 1.5,
		0.7,
		Tween.TRANS_SINE, 
		Tween.EASE_IN_OUT
	)
	tween.start()
	_is_animating = true

func _change_control(tween, node, new_transform):
	tween.interpolate_property(
		node,
		"translation",
		node.translation,
		-node.translation,
		animation_time,
		Tween.TRANS_SINE, 
		Tween.EASE_IN_OUT
	)
	tween.interpolate_property(
		node,
		"rotation",
		node.rotation,
		new_transform,
		0.0,
		Tween.TRANS_SINE, 
		Tween.EASE_IN_OUT,
		animation_time / 2
	)

func _animate_material(tween, material):
	tween.interpolate_property(
			material,
			"albedo_color",
			material.albedo_color,
			Color(1, 1, 1, 0.0),
			1 * animation_time / 3,
			Tween.TRANS_SINE, 
			Tween.EASE_IN_OUT
	)
	tween.interpolate_property(
		material,
		"albedo_color",
		Color(1, 1, 1, 0.0),
		Color(1, 1, 1, 0.7),
		animation_time / 2,
		Tween.TRANS_SINE, 
		Tween.EASE_IN_OUT,
		2 * animation_time / 3
	)

func _tween_all_completed():
	_is_animating = false
	if _prev_node != null :
		_prev_node.translation = _prev_translation
		_prev_node.scale = Vector3(1.0, 1.0, 1.0)
		_prev_node = null
		_prev_translation = null
	if not _next_controls_mapping.empty():
		update_controls(_next_controls_mapping.front())
		_next_controls_mapping.pop_front()
		_next_animation_time.pop_front()
	elif _next_swipe_direction != null:
		animate_direction(_next_swipe_direction)
