extends Spatial

# signal modifier_acquired

const TileType = TileDescription.Tile
const buffer_width = 16
const buffer_length = 16
const tile_size = 2.0
const basic_tile_hight = 20.0

# var floating_text_scene = preload("res://Assets/TextViewport.tscn") 
var tile_scene = preload("res://Source/ArcadeWorld/Grid/Tiles/Tile.tscn")
var vanishing_tile_scene = preload("res://Source/ArcadeWorld/Grid/Tiles/VanishingTile.tscn")
var crystal_scene = preload("res://Source/ArcadeWorld/Grid/Modifiers/Crystal.tscn")
# var text_height = 3.5

var _grid = _create_2d_array(buffer_width, buffer_length, null)
var _clearing = false

onready var clear_timer = $ClearTimer
onready var controller = get_parent()

func _ready():
	clear_timer.connect("timeout", self, "_grid_cleared")

func world_to_grid(position):
	var v = position / tile_size
	return Vector3(floor(v.x), position.y, floor(v.z))

func grid_to_world(position):
	var v = position
	return Vector3(floor(v.x * tile_size), position.y, floor(v.z * tile_size))

func clear():
	var tiles = get_tree().get_nodes_in_group("tiles")
	if tiles.empty():
		return
	_clearing = true
	_remove_all_tiles()
	_remove_crystals()
	clear_timer.set_wait_time(1.0 / Tile.speed_min)
	clear_timer.start()

func render_tiles_at_offset(row, start, offset):
	if _clearing:
		return
	for x in range(start, start + row.size() + 1):
		_set_tile(Vector3(x, 0, offset), row[x - start])
	var last_tile_to_be_rendered = get_tree().get_nodes_in_group("tiles").back()
	yield(last_tile_to_be_rendered, "finished_rising")
	controller.handle_grid_finished_rendering()

func clear_tiles_at_offset(buffer_width, start, offset, anim_time):
	for x in range(start, start + buffer_width + 1):
		_set_tile(Vector3(x, 0, offset), TileDescription.new())
	start_dissapear_animation_for_tiles_at_offset(offset + 1, anim_time)
	controller.handle_grid_changed()

func get_tile(position):
	var position_mod = _mod(position)
	var tile = _grid[position_mod.z][position_mod.x]
	return tile

func activate_tile(position):
	var position_mod = _mod(position)
	var tile = _grid[position_mod.z][position_mod.x]
	if tile == null:
		return
	# tile.visited = true
	for child in tile.get_children():
		if child is Crystal:
			child.disappear()
	if tile.id == TileType.VANISHING:
		tile.vanish()
		tile.connect("finished_vanishing", self, "_tile_vanished")

# func is_tile_visited(position):
# 	var position_mod = _mod(position)
# 	var tile = _grid[position_mod.z][position_mod.x]
# 	return tile.visited

# func display_tile_score(position, score):
# 	var position_mod = _mod(position)
# 	var item = _grid[position_mod.z][position_mod.x]
# 	var text = floating_text_scene.instance()
# 	text.amount = score
# 	add_child(text)
# 	text.translation = Vector3(item.translation.x, item.translation.y + text_height + item.get_aabb().size.z / 2.0, item.translation.z)

func start_dissapear_animation_for_tiles_at_offset(offset, anim_time):
	for x in range(0, buffer_width):
		var position_mod = _mod(Vector3(x, 0, offset))
		var tile =  _grid[position_mod.z][position_mod.x]
		if tile != null:
			tile.dissapear(anim_time)

func _set_tile(position, tile):
	match tile.tile_id:
		TileType.EMPTY:
			_remove_tile_if_present(position)
		TileType.BASIC:
			_show_tile(position, tile_scene, basic_tile_hight, tile.modifier)
		TileType.VANISHING:
			_show_tile(position, vanishing_tile_scene, basic_tile_hight, tile.modifier)

func _show_tile(position, scene, rising_height=0.0, modifier=TileDescription.Modifier.NO):
	_remove_tile_if_present(position)
	var tile = _add_scene(scene, modifier)
	var position_mod = _mod(position)
	_grid[position_mod.z][position_mod.x] = tile
	var world_position = grid_to_world(position)
	var zero_y = world_position.y - tile.get_aabb().size.z / 2.0
	var zero_position = Vector3(world_position.x, zero_y, world_position.z)
	tile.translation = Vector3(zero_position.x, zero_position.y - tile.get_aabb().size.z - rising_height, zero_position.z)
	tile.rise(zero_position)
	return tile

func _remove_tile_if_present(position):
	var position_mod = _mod(position)
	if _grid[position_mod.z][position_mod.x] != null:
		_remove_tile(_grid[position_mod.z][position_mod.x], position)
		_grid[position_mod.z][position_mod.x].remove_from_group("tiles")
		_grid[position_mod.z][position_mod.x] = null

func _remove_tile(tile, position):
	var world_position = grid_to_world(position)
	tile.rise(Vector3(world_position.x, world_position.y - tile.get_aabb().size.z - basic_tile_hight, world_position.z))
	tile.connect("finished_rising", self, "_tile_removed")

func _add_scene(scene, modifier):
	var tile = scene.instance()
	tile.add_to_group("tiles")
	add_child(tile)
	if modifier == TileDescription.Modifier.CRYSTAL:
		_add_crystal_to_tile(tile)
	return tile

func _remove_crystals():
	var crystals = get_tree().get_nodes_in_group("crystals")
	get_tree().call_group("crystals", "dissappear")
	for crystal in crystals:
		crystal.remove_from_group("crystals")
		remove_child(crystal)
		crystal.queue_free()

func _remove_all_tiles():
	var tiles = get_tree().get_nodes_in_group("tiles")
	for tile in tiles:
		var position = world_to_grid(tile.translation)
		_set_tile(Vector3(position.x, 0, position.z), TileDescription.new())

func _add_crystal_to_tile(tile):
	var crystal = crystal_scene.instance()
	crystal.add_to_group("crystals")
	tile.add_child(crystal)
	var crystal_zero_position = Vector3(0, 1.0 + tile.get_aabb().size.z / 2.0, 0)
	crystal.translation = crystal_zero_position
	# crystal.connect("crystal_vanished", self, "_crystal_vanished")

# should refactor this into a separate util script
func _create_2d_array(buffer_width, height, value):
	var a = []
	for y in range(height):
		a.append([])
		a[y].resize(buffer_width)
		for x in range(buffer_width):
			a[y][x] = value
	return a

func _mod(vec):
	return Vector3(int(vec.x) % buffer_width, int(vec.y), int(vec.z) % buffer_length)

func _tile_removed(tile):
	# if not _tile_exists(tile):
	# 	return
	remove_child(tile)
	tile.queue_free()
	controller.handle_grid_changed()

func _tile_vanished(tile):
	# if not _tile_exists(tile):
	# 	return
	var tile_position = tile.transform.origin
	var tile_grid_position = world_to_grid(tile_position)
	_remove_tile_if_present(tile_grid_position)
#	var tile_position_mod = _mod(tile_grid_position)
#	tile.remove_from_group("tiles")
#	_grid[tile_position_mod.z][tile_position_mod.x] = null
#	remove_child(tile)
#	tile.queue_free()
#	controller.handle_grid_changed()

# func _tile_exists(tile):
# 	if get_tree().get_nodes_in_group("tiles").has(tile):
# 		return true
# 	else:
# 		return false

func _grid_cleared():
	clear_timer.stop()
	controller.handle_grid_cleared()
	_clearing = false
	
# func _crystal_vanished():
# 	emit_signal("modifier_acquired")
