extends Timer

func restart(wait_time):
	stop()
	set_wait_time(wait_time)
	start()
