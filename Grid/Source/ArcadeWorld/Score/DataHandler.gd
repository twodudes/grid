extends Node

func save_data(max_score, crystals):
	var data = _to_dict(max_score, crystals)
	var save_game = File.new()
	save_game.open("user://score.save", File.WRITE)
	save_game.store_line(to_json(data))
	save_game.close()

func load_data():
	var save_game = File.new()
	save_game.open("user://score.save", File.READ)
	var contents = save_game.get_as_text()
	var json_result = JSON.parse(contents).result
	save_game.close()
	if json_result == null:
		return {
			"max_score": 0,
			"crystals" : 0
		} 
	return {
		"max_score": json_result["max_score"],
		"crystals" : json_result["crystals"]
	}

func _to_dict(max_score, crystals):
	return {
		"max_score": max_score,
		"crystals" : crystals
	}
