//
// Created by Mikhail Rakhmanovc on 21/08/2020.
// Copyright (c) 2020 Two Dudes. All rights reserved.
//

import Foundation

extension TileType: Codable {}

struct CodableTile: Codable {
    let type: TileType
    let patterns: [MovementPattern]
}

extension CodableTile {
    init(tile: Tile) {
        self.type = tile.tileType
        self.patterns = tile.controls
    }

    func toTile() -> Tile {
        switch self.type {
        case .vanishing:
            return VanishingTile(controls: patterns)
        case .basic:
            return BasicTile(controls: patterns)
        case .moving:
            return MovingTile(controls: patterns)
        case .start:
            return StartTile(controls: patterns)
        case .end:
            return EndTile(controls: patterns)
        case .none:
            return EmptyTile(controls: patterns)
        }
    }
}

struct Level: Codable {
    let width: Int
    let height: Int
    let tiles: [CodableTile]
}

extension Level {
    init(grid: Grid) {
        self.width = grid.width
        self.height = grid.height
        self.tiles = grid.tiles.map { CodableTile(tile: $0) }
    }
}

final class LevelFileEditor {

    let fileManager = FileManager.default

    func save(level: Level, path: String) {
        let encoder = JSONEncoder()
        if let data = try? encoder.encode(level) {
            fileManager.createFile(atPath: path, contents: data)
        }
    }

    func load(path: String) -> Level? {
        print(fileManager.contents(atPath: path))
        return fileManager.contents(atPath: path).flatMap {
            print($0)
            return try? JSONDecoder().decode(Level.self, from: $0)
        }
    }
}